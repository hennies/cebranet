# CebraNET

Trained model to segment membranes in room temperature volume SEM datasets

## Input

The input data needs to be normalized to 8bit unsigned integer values. The model works best if the grey values are spread over the range from 0 to 255.

## Output

A membrane/boundary prediction in the value range from 0 to 1 (dtype=float32).
